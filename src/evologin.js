/**
 * Login To Kplay
 * 
 */
const axios = require('axios')
const cookie = require('cookie')

async function saveData(room, session) {
    
    await axios.get('http://103.31.13.236:1001/evolution/insertEvoSession.asp', {params: {
        roomno: room.number,
        session: session
    }}).then(() => {
        console.log('\x1b[33m%s\x1b[0m', '====== EVO SESSION ======')
        console.log(`Account : ${room.number}`)
        console.log(`Token: ${session}`)
        console.log('\x1b[33m%s\x1b[0m', '========== END ==========')
        console.log('\r\n')
    })
    .catch(err => console.log(err))
}

async function login(room) {

    console.log('Getting Launch URL...')

    const kplay = await axios.post('http://play7979.com/api/kplay/v1/launch', {
        site: 'oceanhole',
        username: room.username,
        game: 1,
        type: 7,
        mobile: 0
    }).then(res => res.data)

    if(kplay.launch_url) {

        console.log('Launch URL Received!')
        console.log('Fetching Evolution Session Token...')

        const token = await axios({
            method: "get",
            url: kplay.launch_url + "&cc=1", //add cc=1 to get set-cookie from the evolution url
            maxRedirects: 0,
            headers: {
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.102 Safari/537.36 Vivaldi/2.0.1309.37",
                "Content-Type": "application/x-www-form-urlencoded"
            },
            validateStatus: function (status) {
                return status >= 200 && status < 303; // default to handle 302 requests
            },
        }).then(res => cookie.parse(res.headers['set-cookie'][0]).EVOSESSIONID  ).catch(err => console.log(err)) 

        console.log('Token Session Received!')

        await saveData(room, token)
    }
}

module.exports = login