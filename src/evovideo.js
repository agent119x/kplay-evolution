/**
 * GET EVOLUTION VIDEO WEBSOCKET
 *
 */
const Websocket = require('ws')
const { performance } = require('perf_hooks')

const getVideoToken = require('../lib/getVideoToken')

let reqId = 0

// Evolution Video EVENT Creator
function eventCreator(type, token) {

    let eventObj = {

        eventType: type,
        timeStamp: performance.now(),
        requestId: reqId++
    }

    if(type === 'AUTHENTICATE') {
        eventObj.videoToken = token
    } else if (type === 'PLAY') {
        eventObj.stream = 'bac5_bi_hi'
    }

    return JSON.stringify(eventObj)
}

async function main(room, sessionId, evoWSpath, broadcaster) {

    let videoToken = await getVideoToken(room)
    let interval = null

    const EVO_VIDEO_URL = `wss://seoa-wow-e01.egcvi.com/app/30/${evoWSpath}/websocketstream?videoSessionId=${sessionId}&videoToken=${videoToken}`

    const ws = new Websocket(EVO_VIDEO_URL)
    ws.binaryType = 'arraybuffer'

    ws.on('open', () => {
        ws.send(eventCreator('PING'))
        ws.send(eventCreator('PING'))

        ws.send(eventCreator('PING'))
        ws.send(eventCreator('PLAY'))

        setInterval(() => {
            ws.send(eventCreator('PING'))
        }, 1000 * 10)

        setInterval(() => {
            ws.send(eventCreator('AUTHENTICATE', videoToken))
        }, 1000 * 60)
    })

    ws.on('message', data => {

        if(data instanceof ArrayBuffer) {

            // Broadcast Video Data
            broadcaster.clients.forEach(function each(client) {
                
                client.send(data);
            })
        } else {
                        console.log(`[${room}] ${data}`)
                }
    })

    ws.on('error', async(err) => {
        console.log(`[${room}] ${err.message}. Restarting...`)
        clearInterval(interval)
        main(room, await getVideoToken(room), evoWSpath, broadcaster)
    })

    // get video token every 1 minute
    interval = setInterval( async() => {
        videoToken = await getVideoToken(room)
    }, 1000 * 10);
}

module.exports = main