/***
 * BACCARAT SHOE DATA
 * 
 */

const axios = require('axios')
const WebSocket = require('ws')

 const getEvoSession = require('../lib/getEvoSession')
 const saveData = require('../config/saveData')
 const inserVideoToken = require('../lib/insertVideoToken')
 const decodeCard  = require('../lib/decodeCard')

 let previousRoomorder = null

 async function getResultHistory(tableLetter) {
    axios.defaults.baseURL = 'http://103.31.13.236:1001';
    return await axios.post(`/evolution/getEvoResultHistory.asp?m=${tableLetter}`).then(res => res.data)
 }

 function getGameStatus(result) {

    let status = 'BET_CLOSED'

    if (result.args.betting === 'BetsClosed' && result.args.dealing === 'Finished' ) {
        status = 'RESULT'
    }
    else if(result.args.betting === 'BetsClosed') {
        status = 'BET_CLOSED'
    }
    else if (result.args.betting === 'BetsOpen') {
        status = 'BET_OPEN'
    }

    return status
 }

 function convertWinner(win, bankerPair, playerPair) {

    let winner = null

	if(win == 'Banker' && !bankerPair && !playerPair ) return 'a';
	else if(win == 'Banker' && !bankerPair && playerPair ) return 'b';
	else if(win == 'Banker' && bankerPair && !playerPair ) return 'c';
	else if(win == 'Banker' && bankerPair && playerPair ) return 'd';
	else if(win == 'Player' && !bankerPair && !playerPair ) return 'e';
	else if(win == 'Player' && !bankerPair && playerPair ) return 'f';
	else if(win == 'Player' && bankerPair && !playerPair ) return 'g';
	else if(win == 'Player' && bankerPair && playerPair ) return 'h';
	else if(win == 'Tie' && !bankerPair && !playerPair ) return 'i';
	else if(win == 'Tie' && !bankerPair && playerPair ) return 'j';
	else if(win == 'Tie' && bankerPair && !playerPair ) return 'k';
	else if(win == 'Tie' && bankerPair && playerPair ) return 'l';

	return 'm'
 }

 async function showResult(roomno, result) {

    const gameStatus = getGameStatus(result)

	const resultHistory = await getResultHistory(roomno)

	result.args.gameNumber = `${result.args.gameNumber}-${resultHistory.length + 1}`

	//result.args.gameNumber = `${result.args.gameNumber}`

    console.log('\x1b[33m%s\x1b[0m', '###### EVO BACCARAT ######')
    console.log(`Table Letter: ${roomno}`)
    console.log(`Game ID: ${result.args.gameId}`)
    console.log(`Game Number: ${result.args.gameNumber}`)
    console.log(`Time: ${result.time} (${ new Date(result.time).toString()})`)
    console.log(`Status: ${gameStatus}`)
	
	if(resultHistory.length > 90) {
	//	console.log('SHUFFLING')
		query = `UPDATE evo_list SET state = 2, data = '', updatedate = GETDATE() where roomno='${roomno}'`
		saveData(query)
	}


    if(gameStatus === 'RESULT') {

        let winner = convertWinner(result.args.gameData.result.winner, result.args.gameData.result.bankerPair, result.args.gameData.result.playerPair)
		let card = decodeCard(result.args.gameData)
    
        console.log(`Winner: ${winner}`)
        console.log(card)

		 query = `UPDATE evo_list SET state = 3, roomorder = '${result.args.gameNumber}', data = data + '${winner}', updatedate = GETDATE() where roomno='${roomno}'` +
			`INSERT evo_room_row(data, roomno, reqdate, roomorder, state, result, card) VALUES ((SELECT data from evo_list where roomno = '${roomno}'), '${roomno}', GETDATE(), '${result.args.gameNumber}', 3, '${winner}', '${JSON.stringify(card)}')`
		
		saveData(query)

	}
    else if(gameStatus === 'BET_OPEN') {
        
        if(previousRoomorder !== result.args.gameNumber) {
           query = `UPDATE evo_list SET state = 1, roomorder = '${result.args.gameNumber}', updatedate = GETDATE() where roomno='${roomno}'` +
			`INSERT evo_room_row(data, roomno, reqdate, roomorder, state, result) VALUES ((SELECT data from evo_list where roomno = '${roomno}'), '${roomno}', GETDATE(), '${result.args.gameNumber}', 1, '13')`
		
			saveData(query)
			
            previousRoomorder = result.args.gameNumber
        }
    }
    else {

		query = `UPDATE evo_list SET state = 2, updatedate = GETDATE() where roomno='${roomno}'` +
		`INSERT evo_room_row(data, roomno, reqdate, roomorder, state, result) VALUES ((SELECT data from evo_list where roomno = '${roomno}'), '${roomno}', GETDATE(), '${result.args.gameNumber}', 2, '0')`
		
		saveData(query)

    }
    console.log('\x1b[33m%s\x1b[0m', '############  END  #############')
    console.log('\r\n')
 }

function getData(roomNo, ins, tableId, sessionId, sesTable) {
    
    const SOCKETURL = `wss://evolution.game-program.com/public/baccarat/player/game/${tableId}/socket?messageFormat=json`
    const instance = `${ins}-${tableId}`
    const ws = new WebSocket(`${SOCKETURL}&instance=${instance}&tableConfig=&EVOSESSIONID=${sessionId}`, { forceNew:true } );

    ws.on('open', function open() {
        ws.send('connected');
      });
       
    ws.on('message', function incoming(data) {
    const result = JSON.parse(data)

        if(result.type === 'connection.kickout') {
            console.log(`[${roomNo}] Session Expired. Restarting...`)
            ws.close()
           
        }

		if(result.type === 'baccarat.encodedShoeState') { // reshuffling
			
			console.log('SHOE HISTORY: ')
			console.log(result.args.history)

			if(parseInt(result.args.history.length) < 4) { // clear
				query = `UPDATE evo_list SET state = 2, data = '', updatedate = GETDATE() where roomno='${roomNo}'`
				saveData(query)
			}
		}


        if(result.type ===  'baccarat.gameState') {
            try {
                if(result.args.betting) {
                    showResult(roomNo, result)
                }
            } catch(e) {
    
            }
        }
        else if (result.type === 'streamSecurity') {
            inserVideoToken(roomNo, result.args.videoToken)
        }
    });
	
	ws.on('close', function() {
		console.log(`[${roomNo} Socket Closed!]`)
		main(roomNo, tableId, ins, sesTable) // re initialize session
	});
	
	ws.on('error', function(err) {
		console.log(`[${roomNo} Error Occured!]`)
		console.log(err)
		 main(roomNo, tableId, ins, sesTable) // re initialize session
	});
}

async function main(roomNo, tableId, instance, sesTable) {

        let sessionID = null

        sessionID = await getEvoSession(sesTable)
        // BACCARAT GET DATA
        getData(roomNo, instance, tableId, sessionID, sesTable)
    
}

module.exports = main