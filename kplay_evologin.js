/**
 * Get Session From KPLAY
 * 
 */
const evologin = require('./src/evologin')
const cluster = require('cluster')
const workers = 1

 const ROOMS = [
     { number: 1, username: 'c1w1g'}, 
     { number: 2, username: 'qkzkfk11' }, 
     { number: 3, username: 'qa200722'},
     { number: 4, username: 'songgeoa'}
 ]

if (cluster.isMaster) {

    console.log('start cluster with %s workers', workers);
    
        let worker = cluster.fork().process;
        console.log('worker %s started.', worker.pid);
    
    cluster.on('exit', function(worker) {
        console.log('worker %s died. restart...', worker.process.pid);
        cluster.fork();
    });
    
} else {

    // run code
    ROOMS.forEach((room) => evologin(room))
    setInterval(() => {
        
        ROOMS.forEach((room) => evologin(room))

    }, 1000 * 60 * 3) // run every 3 minutes
}
