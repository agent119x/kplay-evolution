/**
 * SAVE DATA TO DATABASE
 * 
 */
const axios = require('axios')

// SAVED DATA TO DATABASE
function insertData(query) {
   
    console.log('SQL Query: ')
    console.log(query)

	axios.get('http://103.31.13.236:1002/api/get_active_sites.asp').then(res => {
		
		let sites = res.data

		sites.forEach(site => axios.post(`http://${site.host}:${parseInt(site.port)}/api/md/batch_md_check_main_.asp?data=${encodeURIComponent(query)}`).then()
			.catch(err => console.log(err)))
	})
 }

module.exports = insertData