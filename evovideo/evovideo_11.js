/**
 *  CREATE VIDEO SERVER
 *
 */
const getEvoVideo = require('../src/evovideo')
const videoBroadcaster = require('../lib/videoBroadcaster') //create video broadcaster server
const cluster = require('cluster')

const workers = 1

const TABLES = [
    {roomno: 'EV11', port: 9711, path: 'bac2_bi_med', id: 'oytmvb9m1zysmc44', instance: 'otcia6rtx5ifxnxj-5dacd8fbc91b550fe223079197db0189e6bdb9c48a9501981e3e53c852d025560c7c8403902c3ad8',  buster: '7bfff1'}, // A
]

if (cluster.isMaster) {

    console.log('start cluster with %s workers', workers);

    let worker = cluster.fork().process;
    console.log('worker %s started.', worker.pid);

    cluster.on('exit', function(worker) {
        console.log('worker %s died. restart...', worker.process.pid);
        cluster.fork();
    });

} else {

    TABLES.forEach(table => {
        // evo session ID
        let evoSessionId = `${table.instance}-${table.id}-${table.buster}`
        // create websocker server
        let wss = videoBroadcaster(table.roomno, table.port)
        // get video and rebroadcast
        getEvoVideo(table.roomno, evoSessionId, table.path, wss)
    })
}