<?php
$roomno = $_GET["m"];
$stream = "";

switch($roomno) {

    case "EV1": $stream = "ws://203.245.30.130:9701"; break;
    case "EV2": $stream = "ws://203.245.30.130:9702"; break;
    case "EV3": $stream = "ws://203.245.30.130:9703"; break;
    case "EV4": $stream = "ws://203.245.30.130:9704"; break;
    case "EV5": $stream = "ws://203.245.30.130:9705"; break;
    case "EV6": $stream = "ws://203.245.30.130:9706"; break;
    case "EV7": $stream = "ws://203.245.30.130:9707"; break;

    case "EV8": $stream = "ws://210.114.18.71:9708"; break;
    case "EV9": $stream = "ws://210.114.18.71:9709"; break;
    case "EV10": $stream = "ws://210.114.18.71:9710"; break;
    case "EV11": $stream = "ws://210.114.18.71:9711"; break;
    case "EV12": $stream = "ws://210.114.18.71:9712"; break;
    case "EV13": $stream = "ws://210.114.18.71:9713"; break;
    case "EV14": $stream = "ws://210.114.18.71:9714"; break;
}
?>
<html>
<head></head>
<body>
    <style>
        #videoElement {
            position: absolute;
            z-index: -1;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
    </style>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/flv.js/1.5.0/flv.min.js"></script>
    <video id="videoElement" autoplay="true" muted="muted" preload="auto"></video>
          <button type="button" style="z-index:-1;opacity:0;position: absolute;"></button>
    <script>
        if (flvjs.isSupported()) {

            var videoElement = document.getElementById('videoElement');
            flvPlayer = flvjs.createPlayer({
                "type": 'flv',
                "isLive": true,
                "url": "<?php echo $stream; ?>",
                cors: true,
                enableWorker: true,
                enableStashBuffer: false,
                stashInitialSize: 128,
                autoCleanupSourceBuffer: true,
                lazyLoad: false,
                lazyLoadMaxDuration: 2
            });
            flvPlayer.attachMediaElement(videoElement);
            flvPlayer.load();
            flvPlayer.play();

        }else{

            var videoElement = document.getElementById('videoElement');
            flvPlayer = flvjs.createPlayer({
                "isLive": true,
                "url": "<?php echo $stream; ?>",
                cors: true,
                enableWorker: true,
                enableStashBuffer: false,
                stashInitialSize: 128,
                autoCleanupSourceBuffer: true,
                lazyLoad: false,
                lazyLoadMaxDuration: 2
            });
            flvPlayer.attachMediaElement(videoElement);
            flvPlayer.load();
            flvPlayer.play();
        }
    </script>
</body>
</html>

?>