/**
 *  CREATE VIDEO SERVER
 *
 */
const getEvoVideo = require('../src/evovideo')
const videoBroadcaster = require('../lib/videoBroadcaster') //create video broadcaster server
const cluster = require('cluster')

const workers = 1

const TABLES = [
    {roomno: 'EV13', port: 9713, path: 'bact5_bi_med', id: 'ndgvs3tqhfuaadyg', instance: 'otcm7jfkx5if5toc-324769aebaf852b2115aa553a61b4d30de445c96945599197d49fe0ae386ee2e18d3d43c628855b1',  buster: 'ac43e4'}, // C
]

if (cluster.isMaster) {

    console.log('start cluster with %s workers', workers);

    let worker = cluster.fork().process;
    console.log('worker %s started.', worker.pid);

    cluster.on('exit', function(worker) {
        console.log('worker %s died. restart...', worker.process.pid);
        cluster.fork();
    });

} else {

    TABLES.forEach(table => {
        // evo session ID
        let evoSessionId = `${table.instance}-${table.id}-${table.buster}`
        // create websocker server
        let wss = videoBroadcaster(table.roomno, table.port)
        // get video and rebroadcast
        getEvoVideo(table.roomno, evoSessionId, table.path, wss)
    })
}