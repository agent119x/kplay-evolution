/**
 *  CREATE VIDEO SERVER
 *
 */
const getEvoVideo = require('../src/evovideo')
const videoBroadcaster = require('../lib/videoBroadcaster') //create video broadcaster server
const cluster = require('cluster')

const workers = 1

const TABLES = [
    {roomno: 'EV8', port: 9708, path: 'bact10_bs_med', id: 'nxpj4wumgclak2lx', instance: 'otcia4olfjyfxaun-8982ac1898383f16bf20b707e6e86106135688716e433b272b3a5cd7870df8d8a714b13db04c831b', buster: '530ad0'}, // H
]

if (cluster.isMaster) {

    console.log('start cluster with %s workers', workers);

    let worker = cluster.fork().process;
    console.log('worker %s started.', worker.pid);

    cluster.on('exit', function(worker) {
        console.log('worker %s died. restart...', worker.process.pid);
        cluster.fork();
    });

} else {

    TABLES.forEach(table => {
        // evo session ID
        let evoSessionId = `${table.instance}-${table.id}-${table.buster}`
        // create websocker server
        let wss = videoBroadcaster(table.roomno, table.port)
        // get video and rebroadcast
        getEvoVideo(table.roomno, evoSessionId, table.path, wss)
    })
}