/**
 *  CREATE VIDEO SERVER
 *
 */
const getEvoVideo = require('../src/evovideo')
const videoBroadcaster = require('../lib/videoBroadcaster') //create video broadcaster server
const cluster = require('cluster')

const workers = 1

const TABLES = [
    {roomno: 'EV2', port: 9702, path: 'bac6_bi_med', id: 'lv2kzclunt2qnxo5', instance: 'otchry5ox5ifw4fy-5b8c9b5e004cc983992aa03f5739d481e497b4310f91e96d980fb1230cf377da7023194f3da1aa99', buster: '0c094d'}, // B
]

if (cluster.isMaster) {

    console.log('start cluster with %s workers', workers);

    let worker = cluster.fork().process;
    console.log('worker %s started.', worker.pid);

    cluster.on('exit', function(worker) {
        console.log('worker %s died. restart...', worker.process.pid);
        cluster.fork();
    });

} else {

    TABLES.forEach(table => {
        // evo session ID
        let evoSessionId = `${table.instance}-${table.id}-${table.buster}`
        // create websocker server
        let wss = videoBroadcaster(table.roomno, table.port)
        // get video and rebroadcast
        getEvoVideo(table.roomno, evoSessionId, table.path, wss)
    })
}