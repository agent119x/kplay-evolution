/**
 *  CREATE VIDEO SERVER
 *
 */
const getEvoVideo = require('./src/evovideo')
const videoBroadcaster = require('./lib/videoBroadcaster') //create video broadcaster server
const cluster = require('cluster')

const workers = 1

const TABLES = [

        // SPEED BACCARAT
        {roomno: 'EV1', port: 9701, path: 'bac5_bi_med', id: 'leqhceumaq6qfoug', instance: 'otchry5ox5ifw4fy-f9b92378c6b11847757ba4b0ef6f0047ff3e2f3a8b6e7d90b119d23da5ea848534f46c349fb18d2d', buster: 'e074ba'}, // A
        {roomno: 'EV2', port: 9702, path: 'bac6_bi_med', id: 'lv2kzclunt2qnxo5', instance: 'otchry5ox5ifw4fy-5b8c9b5e004cc983992aa03f5739d481e497b4310f91e96d980fb1230cf377da7023194f3da1aa99', buster: '0c094d'}, // B
        {roomno: 'EV3', port: 9703, path: 'bact1_bs_med', id: 'ndgvwvgthfuaad3q', instance: 'otchry5ox5ifw4fy-5b8c9b5e004cc983992aa03f5739d481e497b4310f91e96d980fb1230cf377da7023194f3da1aa99', buster: '97754d'}, // C
        {roomno: 'EV4', port: 9704, path: 'bact2_bs_med', id: 'ndgvz5mlhfuaad6e', instance: 'otchry5ox5ifw4fy-5b8c9b5e004cc983992aa03f5739d481e497b4310f91e96d980fb1230cf377da7023194f3da1aa99', buster: '4d705d'}, // D
        // 2nd account
        {roomno: 'EV5', port: 9705, path: 'bact3_bs_med', id: 'ndgv45bghfuaaebf', instance: 'otcia4olfjyfxaun-8982ac1898383f16bf20b707e6e86106135688716e433b272b3a5cd7870df8d8a714b13db04c831b', buster: '1a73b6'}, // E
        {roomno: 'EV6', port: 9706, path: 'bact6_bs_med', id: 'nmwde3fd7hvqhq43', instance: 'otcia4olfjyfxaun-8982ac1898383f16bf20b707e6e86106135688716e433b272b3a5cd7870df8d8a714b13db04c831b', buster: '6b1845'},  // F
        {roomno: 'EV7', port: 9707, path: 'bact7_bs_med', id: 'nmwdzhbg7hvqh6a7', instance: 'otcia4olfjyfxaun-8982ac1898383f16bf20b707e6e86106135688716e433b272b3a5cd7870df8d8a714b13db04c831b', buster: '685ca8'}, // G
        {roomno: 'EV8', port: 9708, path: 'bact10_bs_med', id: 'nxpj4wumgclak2lx', instance: 'otcia4olfjyfxaun-8982ac1898383f16bf20b707e6e86106135688716e433b272b3a5cd7870df8d8a714b13db04c831b', buster: '530ad0'}, // H
        // 3rd account
        {roomno: 'EV9', port: 9709, path: 'bact9_bs_med', id: 'nxpkul2hgclallno', instance: 'otcia6rtx5ifxnxj-5dacd8fbc91b550fe223079197db0189e6bdb9c48a9501981e3e53c852d025560c7c8403902c3ad8', buster: '9ea66e'}, // I
        // SQUEEZE BACCARAT
        {roomno: 'EV10', port: 9710, path: 'bac1_bi_med', id: 'zixzea8nrf1675oh', instance: 'otcia6rtx5ifxnxj-5dacd8fbc91b550fe223079197db0189e6bdb9c48a9501981e3e53c852d025560c7c8403902c3ad8',  buster: '6cd63e'},
        // BACCARAT
        {roomno: 'EV11', port: 9711, path: 'bac2_bi_med', id: 'oytmvb9m1zysmc44', instance: 'otcia6rtx5ifxnxj-5dacd8fbc91b550fe223079197db0189e6bdb9c48a9501981e3e53c852d025560c7c8403902c3ad8',  buster: '7bfff1'}, // A
        {roomno: 'EV12', port: 9712, path: 'bac3_bi_med', id: '60i0lcfx5wkkv3sy', instance: 'otcia6rtx5ifxnxj-5dacd8fbc91b550fe223079197db0189e6bdb9c48a9501981e3e53c852d025560c7c8403902c3ad8',  buster: '9a5450'}, // B
        {roomno: 'EV13', port: 9713, path: 'bact5_bi_med', id: 'ndgvs3tqhfuaadyg', instance: 'otcm7jfkx5if5toc-324769aebaf852b2115aa553a61b4d30de445c96945599197d49fe0ae386ee2e18d3d43c628855b1',  buster: 'ac43e4'}, // C
        // LIGHTNING BACCARAT
        {roomno: 'EV14', port: 9714, path: 'lbac_bi_med', id: 'LightningBac0001', instance: 'otcm7jfkx5if5toc-324769aebaf852b2115aa553a61b4d30de445c96945599197d49fe0ae386ee2e18d3d43c628855b1',  buster: 'b8c04a'}
]

if (cluster.isMaster) {

    console.log('start cluster with %s workers', workers);

    let worker = cluster.fork().process;
    console.log('worker %s started.', worker.pid);

    cluster.on('exit', function(worker) {
        console.log('worker %s died. restart...', worker.process.pid);
        cluster.fork();
    });

} else {

    TABLES.forEach(table => {
        // evo session ID
        let evoSessionId = `${table.instance}-${table.id}-${table.buster}`
        // create websocker server
        let wss = videoBroadcaster(table.roomno, table.port)
        // get video and rebroadcast
        getEvoVideo(table.roomno, evoSessionId, table.path, wss)
    })
}