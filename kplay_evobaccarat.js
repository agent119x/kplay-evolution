/**
 * KPLAY EVOBACCARAT
 * 
 */
const baccarat = require('./src/baccarat')
const cluster = require('cluster')
const workers = 1

const TABLES = [
    // SPEED BACCARAT
    {roomno: 'EV1', id: 'leqhceumaq6qfoug', instance: '7c48h-otchry5ox5ifw4fy', sesTable: '1'}, // A
    {roomno: 'EV2', id: 'lv2kzclunt2qnxo5', instance: '8xskp-otchry5ox5ifw4fy', sesTable: '1'}, // B
    {roomno: 'EV3', id: 'ndgvwvgthfuaad3q', instance: 'ncwu2-otchry5ox5ifw4fy', sesTable: '1'}, // C
    {roomno: 'EV4', id: 'ndgvz5mlhfuaad6e', instance: 'ww9zt-otchry5ox5ifw4fy', sesTable: '1'}, // D
    // 2nd account
    {roomno: 'EV5', id: 'ndgv45bghfuaaebf', instance: '7cu73-otcia4olfjyfxaun', sesTable: '2'}, // E
    {roomno: 'EV6', id: 'nmwde3fd7hvqhq43', instance: 'k85mm-otcia4olfjyfxaun', sesTable: '2'},  // F
    {roomno: 'EV7', id: 'nmwdzhbg7hvqh6a7', instance: 'lcl6l-otcia4olfjyfxaun', sesTable: '2'}, // G
    {roomno: 'EV8', id: 'nxpj4wumgclak2lx', instance: '5gjo5-otcia4olfjyfxaun', sesTable: '2'}, // H
    // 3rd account
    {roomno: 'EV9', id: 'nxpkul2hgclallno', instance: '8a4dp-otcia6rtx5ifxnxj', sesTable: '3'}, // I
    // SQUEEZE BACCARAT
    {roomno: 'EV10', id: 'zixzea8nrf1675oh', instance: 'tetqf-otcia6rtx5ifxnxj',  sesTable: '3'},
    // BACCARAT
	{roomno: 'EV11', id: 'oytmvb9m1zysmc44', instance: 'zr0c0-otcia6rtx5ifxnxj',  sesTable: '3'}, // A
    {roomno: 'EV12', id: '60i0lcfx5wkkv3sy', instance: 'jr4ef-otcia6rtx5ifxnxj',  sesTable: '3'}, // B
    {roomno: 'EV13', id: 'ndgvs3tqhfuaadyg', instance: 'hxp2l-otcm7jfkx5if5toc',  sesTable: '4'}, // C
    // LIGHTNING BACCARAT
    {roomno: 'EV14', id: 'LightningBac0001', instance: 'mbqp0-otcm7jfkx5if5toc',  sesTable: '4'}
]

if (cluster.isMaster) {
    console.log('start cluster with %s workers', workers);
    
        let worker = cluster.fork().process;
        console.log('worker %s started.', worker.pid);
    
    cluster.on('exit', function(worker) {
        console.log('worker %s died. restart...', worker.process.pid);
        cluster.fork();
    });
} else {
    // Initiate Tables
    TABLES.forEach(table => {
        //get table data
        baccarat(table.roomno, table.id, table.instance, table.sesTable)
    });
}