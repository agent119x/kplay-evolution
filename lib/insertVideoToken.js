/***
 * INSERT VIDEO TOKEN
 * 
 */
const axios = require('axios')

function insertToken(room, token) {
    axios.get('http://103.31.13.236:1001/evolution/insertVideoToken.asp', { params: {
        roomno: room, token: token
    }}).then(() => {
        console.log('\x1b[36m%s\x1b[0m', `##### [${room}] VIDEO TOKEN #####`);
        console.log(token)
        console.log('\x1b[36m%s\x1b[0m', '################ END ############')
        console.log('\r\n')
    }).catch(err => console.log(err))
}

module.exports = insertToken