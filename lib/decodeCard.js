/***
 * DECODE CARD
 * 
 */

 function decodeCard(cards) {
	


	return [
		 {
			type: 'player',
			cards: cards.playerHand.cards,
			score: cards.playerHand.score
		 },
		 {
			type: 'banker',
			cards: cards.bankerHand.cards,
			score: cards.bankerHand.score
		 }
	]
 }

 module.exports = decodeCard
