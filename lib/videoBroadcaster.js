/**
 * VIDEO BROADCASTER
 *
 */
const Websocket = require('ws')
const fs = require('fs')

function createServer(roomno, port) {

    //read file
    const wss = new Websocket.Server({ port: port })
    
    wss.binaryType = 'arraybuffer'

    console.log(`[${roomno}] Websocket Server Created on PORT: ${port}`)

    wss.on('connection', (ws, req) => {

        console.log(`=== [${roomno}] EVOLUTION VIDEO BROADCAST ===`)
        console.log(`Connected Clients: ${wss.clients.size}`)
        console.log(`Time: ${new Date()}`)
        console.log(`=============== END  =============`)

    fs.readFile( __dirname + '/flvMetaData.txt', function (err, data) {
        if (err) {
            throw err;
        }
        if(roomno == 'EV10' || roomno == 'EV11' || roomno == 'EV12') {
            ws.send(Buffer.from("RkxWAQUAAAAJAAAAABIAAnJE9YoLAAAAAgAKb25NZXRhRGF0YQMABlNlcnZlcgIALk5HSU5YIFJUTVAgKGdpdGh1Yi5jb20vYXJ1dC9uZ2lueC1ydG1wLW1vZHVsZSkADHZpZGVvY29kZWNpZABAHAAAAAAAAAAMYXVkaW9jb2RlY2lkAEAkAAAAAAAAAAdwcm9maWxlAgDA77+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+9AAVsZXZlbAIAwO+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/vQAJZnJhbWVyYXRlAEA3+dsi0OVgAANmcHMAQDf52yLQ5WAADXZpZGVvZGF0YXJhdGUAQHhqAAAAAAAADWF1ZGlvZGF0YXJhdGUAQE9AAAAAAAAAB2dvcHNpemUAQDgAAAAAAAAAAAkAAAJ9CQAALET1igsAAAAXAAAAAAFkAB7/4QAYZ2QAHqyygbB7zcIAAAMA+gAALtUeLFywAQAEaOkzywAAADcIAAAERPWKCwAAAK8AEhAAAAAP", 'base64'))
        } else if(roomno == 'EV14') {
            ws.send(Buffer.from('RkxWAQUAAAAJAAAAABIAAnKDbJIQAAAAAgAKb25NZXRhRGF0YQMABlNlcnZlcgIALk5HSU5YIFJUTVAgKGdpdGh1Yi5jb20vYXJ1dC9uZ2lueC1ydG1wLW1vZHVsZSkADHZpZGVvY29kZWNpZABAHAAAAAAAAAAMYXVkaW9jb2RlY2lkAEAkAAAAAAAAAAdwcm9maWxlAgDA77+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+9AAVsZXZlbAIAwO+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/vQAJZnJhbWVyYXRlAEA3+dsi0OVgAANmcHMAQDf52yLQ5WAADXZpZGVvZGF0YXJhdGUAQH6EgAAAAAAADWF1ZGlvZGF0YXJhdGUAQE9AAAAAAAAAB2dvcHNpemUAQCgAAAAAAAAAAAkAAAJ9CQAALINskhAAAAAXAAAAAAFkAB7/4QAXZ2QAHqy2BsHvNwgAAAMD6AAAu1R4sXcBAAVo6syyLAAAADcIAAAEg2ySEAAAAK8AEhAAAAAP', 'base64'))
        }  
        else {
            ws.send(Buffer.from(data.toString(), "base64"))
        }
    });
})

return wss
}

module.exports = createServer

