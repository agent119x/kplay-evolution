/***
 * GET LATEST EVO SESSION
 * 
 */
 const axios = require('axios')

 async function main(room) {

    const session = await axios.get('http://103.31.13.236:1001/evolution/getEvoSession.asp', {params: {
        roomno: room
    }}).then(res => res.data.session).catch(err => console.log(err))

    console.log('\x1b[33m%s\x1b[0m', '====== EVO SESSION ======')
    console.log(`Table : ${room}`)
    console.log(`Session: ${session}`)
    console.log('\x1b[33m%s\x1b[0m', '========== END ==========')
    console.log('\r\n')

    return session
 }

 module.exports = main